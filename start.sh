#!/bin/bash

RELAY_URL="${1}"
MASTER_URL="${2:-wss://master.timing71.org:8443/ws}"
CROSSBAR_LOG_LEVEL=${CROSSBAR_LOG_LEVEL:-info}

if [[ -z "${RELAY_URL}" ]]; then
    echo "No RELAY_URL given! This must be set to the public URL of this relay node."
    exit 1
fi

echo "Configuring relay for ${RELAY_URL}..."

sed -s "s#%RELAY_URL%#${RELAY_URL}#g; s#%MASTER_URL%#${MASTER_URL}#g" \
    /relay/config.json.template > /relay/config.json

crossbar start --cbdir /relay --loglevel ${CROSSBAR_LOG_LEVEL}
