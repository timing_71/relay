FROM crossbario/crossbar

COPY . /relay
USER root
RUN pip install -e /relay --no-cache-dir

ENTRYPOINT exec /relay/start.sh "$RELAY_URL" "$MASTER_URL"
