#!/bin/bash

set -e

echo -n 'Checking for virtualenv...'
if ! [ -x "$(command -v virtualenv)" ]; then
  echo ' not found'
  if [ -x "$(command -v pip)" ]; then
    echo 'Running "sudo pip install virtualenv"...'
    sudo pip install virtualenv
  else
    echo 'virtualenv not found (and no pip either) - please install manually and run this script again'
    exit 1
  fi
else
  echo ' found'
fi

echo -n 'Checking for openssl...'
if ! [ -x "$(command -v virtualenv)" ]; then
  echo ' not found'
  echo 'Please ensure that openssl is installed and executable.'
  exit 1
else
  echo ' found'
fi

echo -n 'Checking for socat...'
if ! [ -x "$(command -v socat)" ]; then
  echo ' not found'
  echo 'Please ensure that socat is installed and executable.'
  exit 1
else
  echo ' found'
fi

echo 'Enter the fully-qualified domain name of this relay host. This MUST'
echo 'resolve to the current machine, as it will be used to generate SSL'
echo 'certificates required for relay operation.'
read fqdn

if [ -z "${fqdn}" ]; then
  echo 'FQDN must be specified. Terminating.'
  exit 1
fi

echo 'Specify HTTPS port to use (ENTER for default 443)'
read port
port=${port:-443}

echo 'Specify the relay key James has given you'
read relay_key

venv_path="$(pwd)/livetiming-relay"

echo "Creating virtualenv at ${venv_path}..."
virtualenv "${venv_path}"

echo 'Installing livetimingrelay and dependencies...'
"${venv_path}"/bin/pip install livetimingrelay

echo 'Getting acme.sh...'
curl -o "${venv_path}"/bin/acme.sh https://raw.githubusercontent.com/Neilpang/acme.sh/master/acme.sh
chmod a+x "${venv_path}"/bin/acme.sh

echo "Generating SSL certificates for ${fqdn}..."

"${venv_path}"/bin/acme.sh --issue --standalone -d "${fqdn}"

echo 'Installing certificates...'
mkdir -p "${venv_path}/ssl"

"${venv_path}"/bin/acme.sh --install-cert -d "${fqdn}" \
  --cert-file "${venv_path}/ssl/cert.pem" \
  --key-file "${venv_path}/ssl/key.pem" \
  --fullchain-file "${venv_path}/ssl/chain.pem"

echo 'Generating Diffie-Hellman parameters. This may take a while...'
openssl dhparam -2 4096 -out "${venv_path}/ssl/dhparam.pem"

echo "Generating Crossbar configuration file..."
cat <<EOF > "${venv_path}"/relay-config.json
{
  "version": 2,
  "controller": {
    "id": "lt_relay"
  },
  "workers": [
    {
      "type": "router",
      "realms": [
        {
          "name": "timing",
          "roles": [
            {
              "name": "anonymous",
              "permissions": [
                {
                  "uri": "livetiming",
                  "match": "prefix",
                  "allow": {
                    "call": true,
                    "register": false,
                    "publish": false,
                    "subscribe": true
                  },
                  "disclose": {
                    "caller": false,
                    "publisher": false
                  }
                }
              ]
            },
            {
              "name": "relay",
              "permissions": [
                {
                  "uri": "",
                  "match": "prefix",
                  "allow": {
                    "call": true,
                    "register": true,
                    "publish": true,
                    "subscribe": true
                  },
                  "disclose": {
                    "caller": false,
                    "publisher": false
                  }
                }
              ]
            }
          ]
        }
      ],
      "transports": [
        {
          "type": "websocket",
          "endpoint": {
            "type": "tcp",
            "port": ${port},
            "tls": {
              "key": "${venv_path}/ssl/key.pem",
              "certificate": "${venv_path}/ssl/cert.pem",
              "chain_certificates": [
                "${venv_path}/ssl/chain.pem"
              ],
              "dhparam": "${venv_path}/ssl/dhparam.pem",
              "ciphers": "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AES:RSA+3DES:!ADH:!AECDH:!MD5:!DSS"
            }
          },
          "url": "wss://${fqdn}:${port}/ws",
          "auth": {
            "anonymous": {
              "type": "static",
              "role": "anonymous"
            }
          }
        }
      ],
      "components": [
        {
          "type": "class",
          "classname": "livetimingrelay.RelaySession",
          "realm": "timing",
          "role": "relay",
          "extra": {
            "id": "wss://${fqdn}:${port}/ws",
            "event_prefix": "livetiming",
            "remote_realm": "timing",
            "remote_router": "wss://master.timing71.org:8443/ws"
          }
        }
      ]
    }
  ]
}
EOF

echo 'Creating run script...'

cat <<EOF > "${venv_path}"/bin/livetiming-relay
#!/bin/sh
LIVETIMING_RELAY_KEY="${relay_key}" "${venv_path}"/bin/crossbar start --config "${venv_path}"/relay-config.json
EOF
chmod a+x "${venv_path}"/bin/livetiming-relay

echo "Setup complete! Run "${venv_path}"/bin/livetiming-relay to start the relay."
